## INTRODUCTION

Ce guide vous explique comment monter et configurer un disque SSD externe pour être utilisé comme stockage sur un Raspberry Pi.

1. Branchez le disque SSD au Raspberry Pi.

2. Ouvrez un terminal et utilisez la commande `lsblk` pour vérifier que le disque SSD a bien été détecté par la Raspberry Pi.

3. Créez un dossier qui servira de point de montage pour le disque SSD par exemple : 

```bash
sudo mkdir /mnt/ssd
```

4. Utilisez la commande `sudo blkid` pour trouver l'identifiant du système de fichiers du disque SSD. Vous devriez voir une entrée qui ressemble à `/dev/sda1: UUID="[uuid]" TYPE="[type]"`. Notez l'identifiant UUID et le type de système de fichiers (par exemple, ext4 ou ntfs).

5. Ouvrez le fichier /etc/fstab en utilisant un éditeur de texte en mode superutilisateur par exemple : 
```bash
sudo nano /etc/fstab
```

6. Ajoutez une ligne qui indique au système de monter le disque SSD au démarrage. La ligne doit être de la forme suivante :
```bash
UUID=[uuid]   /mnt/ssd   [type]   defaults   0   0
```
7. Enregistrez et fermez le fichier `/etc/fstab`.

8. Utilisez la commande `sudo mount -a` pour monter le disque SSD.

9. Utilisez la commande `df -h` pour vérifier que le disque SSD est bien monté. Vous devriez voir une entrée qui correspond au disque SSD et au point de montage que vous avez créé.

### IMPORTANT
Si le disque SSD n'est pas formaté ou s'il n'a pas de partition, vous devrez également effectuer les étapes suivantes :

1. Utilisez la commande `sudo fdisk /dev/sda` (remplacez /dev/sda par le nom de votre disque SSD s'il diffère) pour lancer l'outil de partitionnement fdisk.

2. Utilisez la commande `n` pour créer une nouvelle partition. Suivez les instructions à l'écran pour créer une partition primaire de type 83 (Linux) qui occupe tout l'espace disponible sur le disque.

3. Utilisez la commande `w` pour enregistrer les modifications et quitter fdisk.

4. Utilisez la commande `sudo mkfs -t ext4 /dev/sda1` (remplacez /dev/sda1 par le nom de votre partition s'il diffère) pour formater la partition en utilisant le système de fichiers ext4.

Voilà, votre disque SSD devrait maintenant être correctement monté sur votre Raspberry Pi. 
